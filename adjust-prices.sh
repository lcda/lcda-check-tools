#!/bin/bash

set -euo pipefail
source "$(dirname "$0")/env"

nwn2-adjust-item-prices --itemvalue="$LCDACLISRC/lcda2da.hak/itemvalue.2da" --stats "$LCDADEV"

if (( ! LINUX )); then
	echo "Press [Enter] to close"
	read
fi
