#!/bin/bash

set -euo pipefail
source "$(dirname "$0")/env"

check-resrefs "$LCDADEV"

if (( ! LINUX )); then
	echo "Press [Enter] to close"
	read
fi
